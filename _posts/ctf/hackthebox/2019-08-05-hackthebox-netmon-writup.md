---
layout: post
title:  "HACKTHEBOX NETMON WRITEUP"
Date: 29 Haziran 2019
Author: Süleyman ERGEN
Editor: Hüseyin ALTUNKAYNAK
tags: [hackthebox, netmon]
License: CC BY
categories: [ctf, hackthebox]
---

# HACKTHEBOX NETMON WRITEUP

`Netmon` makinesi bugün emekli oluyor. Bende alelacele write-up yazayım dedim.

## NMAP

```text
┌─[✗]─[hatsat@HATSAT]─[~/MAKINE/htb/machines/netmon]
└──╼ $ sudo nmap -sSVC 10.10.10.152 -o nmap
[sudo] password for hatsat: 
Starting Nmap 7.70 ( https://nmap.org ) at 2019-06-29 14:29 +03
Nmap scan report for 10.10.10.152
Host is up (0.093s latency).
Not shown: 995 closed ports
PORT    STATE SERVICE      VERSION
21/tcp  open  ftp          Microsoft ftpd
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| 02-03-19  12:18AM                 1024 .rnd
| 02-25-19  10:15PM       <DIR>          inetpub
| 07-16-16  09:18AM       <DIR>          PerfLogs
| 02-25-19  10:56PM       <DIR>          Program Files
| 02-03-19  12:28AM       <DIR>          Program Files (x86)
| 06-29-19  07:03AM       <DIR>          Users
|_02-25-19  11:49PM       <DIR>          Windows
| ftp-syst: 
|_  SYST: Windows_NT
80/tcp  open  http         Indy httpd 18.1.37.13946 (Paessler PRTG bandwidth monitor)
|_http-server-header: PRTG/18.1.37.13946
| http-title: Welcome | PRTG Network Monitor (NETMON)
|_Requested resource was /index.htm
|_http-trane-info: Problem with XML parsing of /evox/about
135/tcp open  msrpc        Microsoft Windows RPC
139/tcp open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp open  microsoft-ds Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2019-06-29 14:32:26
|_  start_date: 2019-06-29 13:58:48

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 155.38 seconds
```

Şimdiii. Biraz çıktıyı inceleyelim.

* Ftp portu açık ve anonymous login etkin. Bu demek oluyor ki Ftp ile giriş yapabiliriz. Hatta dikkat ederseniz ftp dizini Windowsun root yani "C" dizini olarak ayarlanmış. 
* 80 portunda `PRTG Network Monitor` çalışıyor.
* Aynı zamanda 129 ve 445 de Smb servisimiz var.

## FTP

Hiç beklemeden ftp den giriyorum. Terminal yerine grafik arayüzünü tercih ederek filezilla programı ile bağlanıyorum. Dizinlerde sırayla dolaşıyorum. Tabiki de ilk bakacağımız yer olan `Users` dizinine bakıyorum. Buradan `Public` dizini altında `user.txt` dosyasını buluyorum.

![netmon ftp](/assets/ctf/hackthebox/netmon/netmon-ftp.png)


```text
user: dd58************************55a5
```

## PRTG NETWORK MONITOR

PRTG Network Monitor ağ trafiğini izlemek için kullanılan bir yazılımdır. [Wikipedia](https://en.wikipedia.org/wiki/PRTG_Network_Monitor)'dan daha fazla bilgi alabilirsiniz.

80 portuna bağlandığımızda prtg login panelini görüyoruz. Fakat elimizde kullanıcı bilgileri yok. Bu yüzden buradan pek bir şey yapamıyorum.

![netmon panel giris](/assets/ctf/hackthebox/netmon/netmon-port-80.png)

En iyisi diğer portlardan araştırmaya devam etmek.

## FTP DEVAM

Şimdi... sistemde prtg nin kurulu olduğunu biliyoruz. Biraz internetten araştırma birazda ftpden araştırma ile bir şeyler bulmaya çalıştım. Bu araştırmalar ile `/ProgramData/Paessler/PRTG Network Monitor` dizininde prtg programının çeşitli dosyalarının bulunduğunu öğrendim. Bu dizine girdiğimizde `RTG Configuration.old.bak` dikkatimi çekiyor. Bir yedek dosyası gibi. Hemen kendi bilgisayarıma çekip incelemeye başladım.

Dosya yaklaşık olarak 30000 satırdan oluştuğu için içinden yararlı bir bilgi bulmak beni biraz kanser etti. Tabiki de "biraz" bir espriydi :)

Uzun bir incelemeden sonra bir parola buldum. Yaşasııııın !!!

![netmon prtg password](/assets/ctf/hackthebox/netmon/netmon-prtg-password.png)

```text
prtgadmin:PrTg@dmin2018
```

Hemen gidip prtg panelinden deniyorum sonuç hüsran. İşe yaramadı.

Sosyal Mühendislik ile alakalı sunumlarda anlatılan bir olay aklıma geldi. Bazı kurumlar bilgisayar parolalarının aylık olarak değişmesini şart koşuyor. Bunun sonucu olarak da parolalar sürekli karmaşık olmaktan çıkıp belli bir algoritma çerçevesinde oluşuyor. Sizin de tahmin edebileceğiniz gibi \<yıl\>\<ay\>\<rastgele_metin> formatında bir parola ortaya çıkabiliyor. rastgele_metin kısmı gözünüzü korkutmasın, kullanıcı o kısıma da kendi ismini, köpeğinin ismini veya ailesinden birinin ismini koyuyor. Bu durumda bu sene ki parolamız aşağıda :)

```text
prtgadmin:PrTg@dmin2019
```

## ROOT

Nihayet prtg paneline giriş yaptık. Bu andan itibaren hem internetten arştırma ile hemde panelden kurcalama yöntemleri ile işe yarar bir şeyler bulmaya çalışıyorum. Bi süre araştırmadan sonra `Setup > Account Settings > Notifications` menüsünde kod çalıştırmak için bir alan olduğunu öğrendim.

![netmon prtg paneli](/assets/ctf/hackthebox/netmon/netmon-prtg-panel.png)

Hemen `Notification` menüsüne gidiyorum. Yeni bir notification oluşturuyorum. Seçeneklerden `Execute Program` menüsünü etkinleştiriyorum. Çıkan alanda `Program File` seçeneğini `outfile.ps1` olarak değiştiriyorum. Burası önemli! Diğer seçeneklerde istediğimiz sonucu alamıyoruz.

![netmon notification execute](/assets/ctf/hackthebox/netmon/netmon-notification-execute.png)

Parametre kısmına ise `C:\Users\Public\hell.txt;type C:\Users\Administrator\Desktop\root.txt > C:\Users\Public\hell.txt` şeklinde gerekli kodu yazıyorum. ";" dan önceki kısma çıktı dosyasını yazıyoruz. Sonraki kısma ise çalıştırmak istediğimiz `cmd` komutunu yazıyoruz. Sonra doysa yönlendirme ile çıktıyı dosyaya kaydediyoruz.

Tabikide bu komutu bulmadan önce birkaç başarısız deneme yapmam gerekti. Önce dizinleri listeleyerek flag in bulunduğu yeri buldum. Sonra ftp den okuyabileceğim bir dizine kaydettim.

![netmon  notifications](/assets/ctf/hackthebox/netmon/netmon-notifications.png)

Notificationu yazdıktan sonra keydetdik. Şimdi sıra çalıştırmada. Notifications listesinden oluşturduğumuz notificationın sağındaki simgeye tıklıyoruz ve çıkan menüden en üstteki simgeye tıklayarak notificationı çalıştırıyoruz.

![netmon run notification](/assets/ctf/hackthebox/netmon/netmon-run-notification-onay.png)

Çalıştırmadan önce bir onay penceresi gelecek. OK diyoruz.

Şimdi notification çalıştı. Ftp den dosyayı kendi bilgisayarımıza almak kaldı.

![netmon root flag](/assets/ctf/hackthebox/netmon/netmon-root-flag-ftp.png)

Filezilladan bilgisayarımıza alıyoruz ve işte flaaaaag.

```text
root: 3018************************67cc
```

## SONSÖZ

Elimden geldiğince netmon makinesini anlatmaya çalıştım. Umarım faydası olur. Sonraki makinede görüşmek üzere :)