---
layout: post
title:  "HACKTHIS SQLINJECTION WRITEUP"
Author: Süleyman ERGEN
tags: [hackthis, ctf]
categories: [ctf, hackthis]
---

# HACKTHIS SQLINJECTION WRITEUP

![hackthis-image](/assets/ctf/hackthis/hackthis-logo.png)

Bu yazımda hackthis sqlinjection sorularını inceleyeceğiz. Fakat unutmadan, eğer sql injection nedir bilmiyorsanız sorulara ve hacthis te bu sorulara uğraşmadıysanız sizin için bir kaç link:

* [w3schools](https://www.w3schools.com/sql/default.asp) sitesinin sql tutorial bölümü.
* [w3schools](https://www.w3schools.com/sql/sql_injection.asp) sitesinin sql injection bölümü.
* [owasp.org](https://www.owasp.org/index.php/Blind_SQL_Injection) sitesinin sql injeection bölümü.

Sql, sqlinjection konularını bildiğinizi ve bu seviyedeki sorulara uğraştığınızı farzederek sorulara gaçiyorum.

## Level 1

Bu seviyede bir giriş ekranı var. Giriş ekranındaki zaafiyeti kullanarak kullanıcı bilgilerini bilmeden giriş yapmamız bekleniyor. username kısmını tek tırnak yapıp giriş yapmaya çalıştığımızda sql hatası alıyoruz. Demekki sqlinjection var.

Veritabanından kontrol yapan sql kodu yaklaşık buna benzemesi gerekiyor:

```sql
SELECT * FROM user_table WHERE username='uname' AND password='upass'
```

Eğer biz uname ve upass zararlı girdi verirsek giriş yapabiliriz. Nasıl mı?
Sql de tek tırnak girdiyi sonlandırır. eğer biz kullanıcı adını `asd'` şeklinde girersek sql sorgusu buna benzer:

```sql
SELECT * FROM user_table WHERE username='asd'' AND password=upass
```

Yani biz bu tek tırnak ile girdiyi sonlandırdık. Fakat sorugu bu şekilde çelışırsa hata verecek çünkü sql syntax'i bozuldu. Bu yüzden kullancı girişine başka şeylerde girmemiz gerekiyor ve sql sonucunun doğru gelmesini sağlamamız gerekiyor. `asd' or 1=1` şeklinde bir girdi ile sorgu sonucu TRUE gelecek. Şifre ne olursa olsun. Tabiki burada asd rasgele bir şey, siz başka bir şeyde yazabilirsiniz. Sorgumuzda hala syntax hatası var çünkü kullanıcıadından sonra hala sorgu devam ediyor. Aşağıdaki gibi:

```sql
SELECT * FROM user_table WHERE username='asd' OR 1=1 AND password=upass
```

Burada `or 1=1` den sonra sql sorugusu devam ediyor. Bunu yorum satırına çavirmemiz gerekiyor. sqlde yorum satırı `--` dir. O zaman son girdimiz: `asd' OR 1=1 -- -` olur. Sql sorgusu ise şu hali alır:

```sql
SELECT * FROM user_table WHERE username='asd' OR 1=1 -- - OR 1=1 and password=upass
```

Şifre olamadan sisteme giriş yaptık. İşte bu kadar :)

## Level 2

Bu soruda admin olarak giriş yapmamız bekleniyor. Ayrıca giriş ekranının hemen altında kullanıcıları görebiliceğimiz bir buton (browse members). Butona tıkladığımızda ve ardından herhangi bir harfe tıkladığımızda url şu şekide değişiyor:
`https://www.hackthis.co.uk/levels/sqli/2?browse&q=a`

Bu demek oluyor ki get metodu ile q parametresi ile sorgulanacak harf gönderiliyor ve o harf ile başlayan üyeler listeleniyor. sql sorgusu buna ber bi şey olması gerekiyor:

```sql
SELECT username,admin FROM members WHERE username LIKE 'A%'
```

Burada A bizim get metodu ile gelen harf. Bu sorguyu işimize yarayacak şekilde değiştirmemiz gerekiyor. O yüzden öncelikle tek tırnak ile girdiyi sonlandırıyoruz. Bu işlemi url üzerinden yapacağız çünkü sayfada veri girişi için alan yok.

```text
https://www.hackthis.co.uk/levels/sqli/2?browse&q=a'
```

Bu şekilde yaptığımızda sql sorgu hatası ekrana geliyor. tabload username ve admin kolonları olduğunu görebiliyoruz. elbette password içinde bir kolon olması gerekiyor. Ardından biraz daha değişiklikle admin kolonu 1 olan yani admin olan kullanıcı ismini alabiliriz:

```text
https://www.hackthis.co.uk/levels/sqli/2?browse&q=%' and admin=1 -- -
```

Sql de yüzde işareti herhangi bir string yerine geçer. Yani harf filtrelemesi olmadan herhangi bir kullanıcı adı. Ardından `and admin=1 -- -` ile admin kolonu 1 olan kullanıcıları ekrana yazdırıyoruz. "bellamond". Yani adminimizin kullanıcı adı bu.

Şimdi şifresini almaya geldi. UNION ALL kullanabiliriz. UNION sql de 2 tabloyu birleştirmek için kullanılır. 2. tablomuz ile select ile başlayan ve url de son kısımda yazdığımız sorgu ile gelen tablo. Bu iki tabloyu birleştirir ve sonucu bize gönderir.

```text
https://www.hackthis.co.uk/levels/sqli/2?browse&q=' UNION ALL SELECT password,admin FROM members WHERE admin=1 -- -
```

Bu sorgu da `q='` ile bir karakter girmediğimiz için buradan çıktı gelmez yani boş gelir. Devamında ise password ve admin kolonlarından admin=1 olan kullanıcıların parolalarını çakiyoruz. burada bize 1 tane sonuç geldi. Buda: `1b774bc166f3f8918e900fcef8752817bae76a37`.

Elbetta tahmin edebileceğiniz gibi bu bir hash. Şifre herhangi bir hash fonksiyonundan geçirilip veriyabanına kaydedilmiş.

Bu hash değerini googleda doğrudan aratarak bulabilirsiniz yada herhangi bir hash decoder sitesine girip bulabilirsiniz. Ben google babaya sorup ilk siteye girdiğimde sonucu `sup3r` olarak, hash fonksiyonunuda SHA1 olarak hemen gördüm. Bize lazım olan bütün bilgileri aldık.
Username: bellamond
Password: sup3r

Bu bilgiler ile seviyeyi geçebiliriz.