---
layout: post
title:  "HACKTHIS CODING"
Autor: Süleyman ERGEN
categories: [ctf, hackthis]
---

# HACKTHIS CODING

Bu kategoride basit kodlama alıştırmaları yapacağız.

## Level 1

Bu seviye bize bir grup kelimeyi sıralamamızı istiyor. Fakat bunu kod yazarak yapmamız lazım çünkü sıralamak için 5 sayiyemiz var.

Ben bu iş için javascript kullandım. Çünkü browserlarda çalışan dir budur. İsterseniz başka yöntemlerle de başka diller kullanabilirsiniz.

İşte benim yöntemimle yazdığım kod.

```javascript
// ilk metin alanındaki değerleri al text değişkenine ata
var text = document.getElementsByTagName("textarea")[0].value;

// text içindeki stringi virgül ve boşluga göre parçala
// bu işlem bir array döndürecek
var arr = text.split(", ");

// arrayi sırala
arr.sort();

// sıralanmış arrayi stringe dönüştür
var answer = arr.toString();

// arrayden stringe dönüştürme sırasında toString fonksiyonu araya virgüller koyar
// faka bizim virgül ve boşluk koyamamız lazım
answer = answer.split(",").join(", ")

// son olarakta bulduğumuz cevabı gerekli alana yerleştirmek kalıyor
document.getElementsByTagName("textarea")[1].value = answer;
```

## Level 2

Bu seviyede bir decode işlemi yapmamız gerekiyor. Algoritma basit bu yüzden doğrudan kodu veriyorum.

```javascript
txt = "";

var text = document.getElementsByTagName("textarea")[0].innerHTML;
words = text.split(",");

// tüm karakterleri dolaş ve dedoce işlemini yap
for(x in words){
    txt = txt + String.fromCharCode(126 - words[x]);
}

// ~ karakterini boşluk ile yer değiştir.
txt = txt.split("~").join(" ");

// küçük harfe çevir ve gerekli input alanına ekle.
document.getElementsByTagName("textarea")[1].innerHTML = txt.toLowerCase();
```