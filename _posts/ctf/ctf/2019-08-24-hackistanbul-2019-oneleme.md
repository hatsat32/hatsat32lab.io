---
layout: post
title:  "Hackistanbul 23 Ağustos Öneleme CTF"
Date: 24 Ağustos 2019
Author: Süleyman ERGEN
tags: [hackistanbul, ctf]
License: CC BY
categories: [ctf]
---

# Hackistanbul 23 Ağustos Öneleme CTF

Hackistanbul için 23 ağustos 2019 da 1. öneleme gerçekleşti. Bizde `MCLee` ekibi olarak katıldık. Ctf oldukça keyifli geçti. Bide buna writeup yazarsak tadından yenmez dedik :)

## Stage 1

---

### Soru 1

![soru1](/assets/ctf/ctf/hackistanbul2019/23agustos/1/soru1.png)

Bu soru basit bir stego sorusuydu. `steghide` aracı ile kolaylıkla bulabildik. Soruda verilen resim şu:

![soru1](/assets/ctf/ctf/hackistanbul2019/23agustos/1/oracle.jpg)

```text
$ steghide extract -sf oracle.jpg 
Enter passphrase: 
wrote extracted data to "steganopayload13580.txt".
$ ls
oracle.jpg  soru1.png  steganopayload13580.txt
$ cat steganopayload13580.txt 
aHR0cHM6Ly9wYXN0ZWJpbi5jb20vUGZMWWRUNGI=
$ cat steganopayload13580.txt | base64 -d
https://pastebin.com/PfLYdT4b
```

Çıkan linke gittiğimizde ise flagi buluyorduk:

```text
1kn0w17551mpl3
```

### Soru 2

![soru2](/assets/ctf/ctf/hackistanbul2019/23agustos/2/soru2.png)

Bu soruda bir pcap dosyası vermiş. Bu dosyadaki flagi bulmamız gerekiyordu. Bu pcap dosyasında network paketlerinden birinin içine yorum konmuştu. Bu yorumda flag vardı. Pcap dosyasını [şuradan](/assets/ctf/ctf/hackistanbul2019/23agustos/2/krb-816.pcapng) indirebilirsiniz.

![soru2-cevap](/assets/ctf/ctf/hackistanbul2019/23agustos/2/soru2-cevap.png)

flag:

```text
easy_as_1_2_3
```

### Soru 3

![soru3](/assets/ctf/ctf/hackistanbul2019/23agustos/3/soru3.png)

Bu soruda bir zip verilmiş ve buradan bir ip adresi bulmamız isteniyor. Zip dosyasına şuradan [indirebilirsiniz](/assets/ctf/ctf/hackistanbul2019/23agustos/3/GreenCMS-2.3.0603.zip).

Normalde dosya yüklemeleri için bir dizin oluşturulur. Genellikle "upload" tarzı isim verilir. Zip dosyasını açtıktan sonra incelerken bide ne göreyim! Upload dizininde `avatar.png.php` isminde bir reverse shell dosyası. IP adreside içinde :) flag:

```text
10.10.10.62
```

### Soru 4

![soru4](/assets/ctf/ctf/hackistanbul2019/23agustos/4/soru4.png)

Bu soruda `delusion.py` isminde bir python dosyası verilmiş. İçindeki kod aşağıdaki gibi:

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

redacted_flag = [948, 205, 938, 216, 934, 214, 940, 57, 860, 33, 850, 56]
FLAG= [ "R", "E", "D", "A", "C", "T", "E", "D"]
if __name__ == "__main__":
    num1=188
    num2=881
    result = 0x0
    i = 1
    for char in FLAG:
        try:
            if i % 2 != 0:
                i += 1
                i  = i ^ num2
            result = char ^ i ^ num1
            redacted_flag.append(result)
        except TypeError:
            print("What did you expect, a flag?")
```

Bu soru bizi biraz uğraştırdı. Başta `TypeError` hatasını düzeltmeye çalıştık. `ord` fonksiyonu ile düzelttiğimizde ortaya anlamlı bi şey çıkmadı.

Kodu biraz incelediğimizde kriptolama tarzı bi işlem yapıp `redacted_flag` değişkenine eklediğimizi gördük. Redacted kelimesinin anlamı ise "sansürlenmiş", "baskıya hazırlanmış" demek. Biraz mantıkla for döngüsüne "sansürlenmiş" veriyi verirsek belki bize flagi verir diye düşündük. Simetrik şifreleme gibi.

`redacted_flag` değişkenini for döngüsüne verip her döngüde `result` değişkenini yazdırdığımızda sonucu bulduk.

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

redacted_flag = [948, 205, 938, 216, 934, 214, 940, 57, 860, 33, 850, 56]
FLAG= [ "R", "E", "D", "A", "C", "T", "E", "D"]
if __name__ == "__main__":
    num1=188
    num2=881
    result = 0x0
    i = 1
    for char in redacted_flag:
        try:
            if i % 2 != 0:
                i += 1
                i  = i ^ num2
            result = char ^ i ^ num1
            print chr(result)
            redacted_flag.append(result)
        except TypeError:
            print("What did you expect, a flag?")
print redacted_flag
```

flag:

```text
{tamagotchi}
```

### Soru 5

![soru5](/assets/ctf/ctf/hackistanbul2019/23agustos/5/soru5.png)

Bu soruda bir `hi.exe` isminde bir exe dosyası vermiş ve içindeki gizli flagi bulmamızı istiyor. Dosyayı [buradan](/assets/ctf/ctf/hackistanbul2019/23agustos/5/hi.exe) indirebilirsiniz.

Dosyayı çalıştırmadan önce `strings` komutu ile baktığımda ilginç bir şekilde sha1 hashi ile karşılaşıyorum.

```text
$ strings hi.exe 
ea01e56d35d1b236cd41bc569e3847811b03c104
```

Hash değeri google da arattığımda bi şey çıkmadı. Birkaç malvare analizi denemesinden sonra hash değerine tekrar döndük. İnternette bazı hash bulma sitelerinden birinde bulduk. site: [hashtoolkit.com](https://hashtoolkit.com). Flag:

flag:

```text
flag{HelloIGotThflag3}
```

### Soru 6

![soru6](/assets/ctf/ctf/hackistanbul2019/23agustos/6/soru6.png)

Soruda bir resim verilmiş ve bizden gizli flagi bulmamız isteniyor. Bizde hemen yola koyulduk.

`steghide` be `binwalk` denemelerimiz sonuç vermedi. Ayrıca resimin metadata bilgilerindende bir şey bulamadık. Resmi `strings` ile incelediğimizde de bir şey çıkmadı.

![h19](/assets/ctf/ctf/hackistanbul2019/23agustos/6/h19.png)

Bunun gibi birkaç denemeden sonra sorunun `zsteg` sorusu olduğunu anladık.

```text
#### ZSteg ####
imagedata .. file: shared library
b1,r,lsb,xy .. text: "<|\\u1l"
b1,r,msb,xy .. text: "|ni=w "
b1,g,lsb,xy .. file: Tower32/600/400 68020 object not stripped - version 27790
b1,b,lsb,xy .. text: "d}[Ww+"
b1,rgb,msb,xy .. file: raw G3 data, byte-padded
b1,bgr,lsb,xy .. text: "\nflag{h-19}"
b1,bgr,msb,xy .. text: "Yb8IJz"
```

Flag:

```text
flag{h-19}
```

### Soru 7

![soru7](/assets/ctf/ctf/hackistanbul2019/23agustos/7/soru7.png)

Bu soruda bir dosya veriyor. Dosyanın içeriği şu şekilde:

```text
..... ..... ..... .!?!! .?... ..... ..... ...?. ?!.?. ..... ..... .....
..... ..... ..!.. ..... ..... .!.?. ..... .!?!! .?!!! !!!?. ?!.?! !!!!.
..... ..... ..!.. .!.!! !!!!! .?... ....! ?!!.? ..... .?.?! .?... .....
!.?.. ..... !?!!. ?!!!! !!?.? !.?!! !!!!! !!.?. 
```

Bu metnin `brainfuck` tarzı bir encoding-language olduğunu tahmin etmiştik. Fakat gerçekte hangi encoding/language olduğunu bulmak oldukça uğraştırdı. Uzun bir araştırmadan sonra bu metnin `okk` diline ait olduğunu bulduk. Hemen [decode.fr okk language](https://www.dcode.fr/ook-language) den bu metni çalıştırıyorum. Yani okk interpreterine veriyorum. Sonuç ise şu:

```text
NTIOPMZM
```

Bunu flag olarak denediğimizde ise hata alıyoruz. Uzun bir uğraştan sonra bu metnin `rot18` ile şifrelendiğini öğreniyoruz. Daha sonra [rot13.com](https://rot13.com/) dan rot18 deşifreleme işlemini yapıyoruz. flag:

```text
FLAGHERE
```

## Stage 2

---

### Soru 8

![soru8](/assets/ctf/ctf/hackistanbul2019/23agustos/8/soru8.png)

Sorudaki metni `base64` ile decode ettikten sonra bir link çıkıyor.

```text
$ echo -n "aHR0cHM6Ly9pbWd1ci5jb20vYS90UkJCaDFF" | base64 -d
https://imgur.com/a/tRBBh1E
```

Ardından hemen linke gidiyorum ve resmi alıyorum.

![soru8](/assets/ctf/ctf/hackistanbul2019/23agustos/8/yeEpks7-Imgur.png)

İnternette biraz araştırmanın ardından bunun `dancing man cipher` olduğunu buluyoruz. Flag:

```text
dancewithme
```

### Soru 9

![soru9](/assets/ctf/ctf/hackistanbul2019/23agustos/9/soru9.png)

Sorudaki zip dosyasını buradan indirebilirsiniz: [queen.zip](/assets/ctf/ctf/hackistanbul2019/23agustos/9/queen.zip)

Soruda verdiği ipucundan `rockyou` olduğunu hemen anladık. Ama rockyou çok uzun süreceğini düşünüp internetten online olarak şifreyi bulmaya çalıştık. Bu pek başarılı olmadı :(

Bu denemelerin ardından `rockyou` denemesi yaptık.

```text
$ zip2john queen.zip > hash
ver 1.0 efh 5455 efh 7875 queen.zip/queen.mp3 PKZIP Encr: 2b chk, TS_chk, cmplen=30, decmplen=18, crc=9C1E1DFC

$ john --wordlist=/usr/share/wordlists/rockyou.txt hash
Using default input encoding: UTF-8
Loaded 1 password hash (PKZIP [32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
lucky7777        (queen.zip/queen.mp3)
1g 0:00:00:00 DONE (2019-08-25 19:31) 9.090g/s 4468Kp/s 4468Kc/s 4468KC/s mrfrosty..losangelitos
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```

Parola: `lucky7777`. Zip dosyasını açıyorum. Ardından içindeki dosyayı.

```text
$ file queen.mp3 
queen.mp3: ASCII text, with no line terminators

$ cat queen.mp3 
w3_w1Ll_r0CKy0U!!!
```

Flag:

```text
w3_w1Ll_r0CKy0U!!!
```
{% comment %}
### Soru 10

![soru10](/assets/ctf/ctf/hackistanbul2019/23agustos/10/soru10.png)
{% endcomment %}

### Soru 11

![soru11](/assets/ctf/ctf/hackistanbul2019/23agustos/11/soru11.png)

Bu soruda dosya olarak `meim.jpg` isminde bir resim vermiş. Resim aşağıda:

![meim jpg](/assets/ctf/ctf/hackistanbul2019/23agustos/11/meim.jpg)

Başta `strings` ile metinleri aldığımda, çıktının başında şunlar geçiyordu:

```text
$ strings meim.jpg | head
JFIF
				
$3br
%&'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz
	#3R
&'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz
7DrC
Uev|
ba({9=
m{a0
```

Genellikle stegonography sorularında sıkça gördüğüm için `steghide` aracı ile birşeyler bulmaya çalıştık.

Parola girmediğimde bir şey vermiyordu. Ardından parola denemeleri yaptık. password, PASSWORD, meim, MEİM, ...

Fakat yarışma esnasında maalesef bulamadık. Yarışmadan sonra baktığımızda parolanın `hackistanbul` olduğunu gördük. Flag:

```text
$ steghide extract -sf meim.jpg -p hackistanbul
wrote extracted data to "flag.txt".
$ cat flag.txt 
{G00dAndW3lcomeToHa3kIstanbul}
```

### Soru 12

![soru12](/assets/ctf/ctf/hackistanbul2019/23agustos/12/soru12.png)

Soruda verilen [dosyayı](/assets/ctf/ctf/hackistanbul2019/23agustos/12/dosya.exe) indirdikten sonra incelemeye başlıyoruz. Fakat cevabın `stings` komutu ile çıkması bizi hayal kırıklığına uğrattı :(

```text
$ strings dosya.exe 
!This program cannot be run in DOS mode.
Rich
.text
. . . . .
. . . . .
. . . . .
. . . . .
70:4:8:<:@:D:H:L:P:T:`:h:
= =$=0=4=8=<=@=D=H=L=T=X=p=
flag{hi19}
```

### Soru 13

![soru13](/assets/ctf/ctf/hackistanbul2019/23agustos/13/soru13.png)

Bu sorudaki dosyayı gördüğümde hexdump olduğunu anladım. Fatat ters çevrilmiş. Dosya içeriği şu şekilde:

```text
65d6 0377 5674 1587 4585 46f4 6575 2585 :00100000
6503 46f6 65c6 9577 a544 2575 2664 c633 :0f000000
75b6 a5f4 6555 1375 3684 0785 1623 d413 :0e000000
65a6 a4b4 2523 a454 45c6 8686 d4b6 0387 :0d000000
65d6 1343 9575 d497 35b6 6555 a026 8424 :0c000000
8565 d687 7755 6546 75a5 4425 45d4 b613 :0b000000
4365 7453 f416 1507 3526 b6a4 0565 7513 :0a000000
4346 2365 75a5 6446 1635 8424 3755 d613 :09000000
3575 6465 47e4 5547 a555 8524 4595 c646 :08000000
f646 c6a5 74a5 8407 b6a0 d4c6 a584 75b6 :07000000
a5b6 9565 a474 35c6 65a5 2664 0784 45c6 :06000000
a516 3574 2574 a564 a516 d403 a477 6575 :05000000
07b4 2623 2537 75b6 4685 2674 2527 34b6 :04000000
1354 65c6 8675 d4e6 8685 65c6 46b4 7565 :03000000
6557 35c6 0786 a0d4 6507 d465 c625 7436 :02000000
2325 8535 8507 1625 c6a4 2745 7507 e6e4 :01000000
6554 9795 a7c6 1555 7583 9334 76d3 d3a0 :00000000
```

Bu texti internette bir araç yardımıyla ters çevirdikten sonra [hexed.it](https://hexed.it/) adresinden metni giriyorum. Karşıma ters çevrilmiş bir base64.

```text

==gC98WUQlzYyEVNnpWTrJlRapXSXR2cGRlVMpVM
hplSuVVWKdlVXhnMWhlVE1kCrRGbXdkWsR2bKpWVwJ0MaZFZGRGSaZlTHpFbZVlSGJVYkZkWHZlM
kpHZGZldodlYTBXUZtUNtVFWS1mUsBHSadFZWV2d41WVPJkbSpQaO5GV41kMTRDZWdVUwxmVXBHb
UVkSyMWY41mVx0kMhhlTEJ2RKJjV1M2aXpHcW1UVOZkW3lFbWRDZwYlVod0VXRWVOdXTxQGew0mV
```

Bunuda ters çeviriyorum. Elimde temiz bir base64 var. Bu base64 metnini 9 defa decode ettikten sonra flag karşımıza çıkıyor.

```text
{its_something}
```
{% comment %}
### Soru 14

![soru14](/assets/ctf/ctf/hackistanbul2019/23agustos/14/soru14.png)
{% endcomment %}

## Stage 3

---

{% comment %}
### Soru 15

![soru15](/assets/ctf/ctf/hackistanbul2019/23agustos/15/soru15.png)

### Soru 16

### Soru 17

### Soru 18
{% endcomment %}

### Soru 19

![soru19](/assets/ctf/ctf/hackistanbul2019/23agustos/19/soru19.png)

Soruda `flag2` isminde bir dosya veriyor. Doyaya `file` aracı ile bakıyorum.

```text
$ file flag2 
flag2: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=7b275bbf7e06ab63e7d8a8b35ee91d1462c2c75f, not stripped
```

Çalıştırılabilir bir dosya. Çalıştırıyorum.

```text
$ ./flag2 
Enter show me the flag  :) :
flag
Really ? do you think yourself smart ass by runing this app and typing view you will get the flag?flag
```

Birkaç başarısız denemenin ardından dosyayı `ghidra` ile açıyorum. Bir süre inceledikten sonra `heyMan` isminde bir fonksiyon görüyorum. Ardından fonksiyonu görüntülüyorum. Atamalardan birinde `{` görünce flagin burda olduğunu anladım.

![soru19](/assets/ctf/ctf/hackistanbul2019/23agustos/19/soru19-ghidra.png)

Geriye kalan tek şey remin sağında hexadecimal olarak yazılan değişkenleri ascii karşılığına dönüştürmek. Flag:

```text
{LIKtotryhArderIAmCo0ool}
```

### Soru 20

![soru20](/assets/ctf/ctf/hackistanbul2019/23agustos/20/soru20.png)

Bu soruda bir pcap dosyası verilmiş. Bu dosyayı incelerken mail (smtp) iletişimi gördüm. Muhtemelen buraya bakmamız gerektiğini anladım. Ardından mail iletişiminde gönderilern dosyaları wireshark ile dışarı aktardım. Dosyaları inceledim fakat bir şey bulamadım.

Ardından wireshark üzerinden string araması yaptım. flag, hackistanbul, tarzı şeyler aradım. Ardından çok geçmeden flagi buldum.

![soru20](/assets/ctf/ctf/hackistanbul2019/23agustos/20/soru20-cevap.png)

flag:

```text
flag-hack-istanbul-ctf
```

---

## Bitirirken

