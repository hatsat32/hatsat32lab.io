---
layout: post
title:  "Codeigniter 4 Public Dizini"
Author: Süleyman ERGEN
tags: [Codeigniter4, ci4]
categories: [codeigniter4]
---

# Codeigniter 4 Public Dizini

İnternette bazı kurulumlarda public dizininin kaldırıldığını gördüm. Bu konu hakkında kendi görüşlerimi şu şekilde anlatmak istiyorum.

Puclic dizini projenizdeki bütün dosyaların (özellikle config ve log dosyaları) dış dünyaya açık olmaması için kullanılır. Public dizini içerisindeki bir tane php dosyası ile gelen bütün istekleri doğru ve düzgün bir şekilde karşılayıp işlemeniz mümkündür. Aynı zamanda public dizinindeki dosyalar kritik öneme sahip olmayan, açığa çıkması gibi bir durumda site sahibine zararı olmayacak dosyaların bulunduğu dizindir.

Diğer açıdan public dizini dışındaki dosyalar yani projenin geri kalan dosyaları proje için ciddi öneme sahiptir ve dışarıya __kati surette__ çıkmaması ve sızdırılamamsı gerekir. Hadi biraz örnek yapalım.

Codeigniter 4'ün dizin yapısını hatırlayın.

```
.
├── app/
├── public/ -> buradan bahsediyorum :)
├── tests/
├── vendor/
├── writable/
├── builds
├── composer.json
├── composer.lock
├── env
├── phpunit.xml.dist
└── spark
```

Şimdi public dizinini kaldırdığınızı ve public dizini içerisindeki dosyaları `index.php`, `.htaccess`, vs dosyaları proje dizinine taşıdığınızı varsayalım. Peki bunun ne gibi problemleri var ????????

## `logs` Dizinini Dış Dünyaya Açıyorsunuz

Bilmeyenler için kısaca anlaymak gerekirse `writable` dizini projenize ait session, log, cache gibi önemli dosyaların bulunduğu dizindir. Eğer public dizinini kaldırısanız ve proje dizinini document root olarak ayarlarsanız bu dizinide dünyaya açmış olacaksınız.

Codeigniter 4 günlük olarak proje ile ilgili loglar tutar. Formatı şu şekildedir. `log-YYYY-MM-DD.log`. YYYY yerine yıl gelecek. 2020 gibi. MM yerine ay gelecek, 03 gibi. DD yerine gün numasarı gelecek, 12 gibi. Mesela _27 mart 2020_ tarihine ait loglar `writable/logs/log-2020-03-27.log` dosyası içerisinde bulunacak. Bir kişi hangi güne ait loglara ulaşmak istiyorsa istediği gibi ulaşabilecek. Saldırganlar için oldukça iştah kabartıcı değil mi :)

## `session` Dizinini Dış Dünyaya Açıyorsunuz

Codeigniter 4 session ile ilgili bilgileri `writable/session/` dizininde tutar. Formatı şu şekilde `ci_session{PHPSESID}`. Bir kullanıcı kendi tarayıcısından kendisi ile ilgili tuttuğunuz session bilgisine oldukça rahat ulaşabilir. Siz istemeseniz bile.

Vahim bir durum aşağıdaki örnekte:

![session](/assets/genel/ci4-public/session.png)

Böyle bir durumda kullanıcı yada saldırgan bilmemesi ve ulaşamaması gereken bir bilgiye ulaşabilir.

## `cache` Dizinini Dış Dünyaya Açıyorsunuz

Web uygulamalarında veritabanını yormamak ve daha hızlı sonuç almak için bir takım şeyleri saklarız. Codeigniter 4 disk tabanlı bir cache mekanizması sunar. Bu sayede web uygulamanızın daha hızlı çalışmasını sağlayabilirsiniz.

Codeigniter 4 cache dosyalarını `writable/cache` dizininde saklar. Dosya isim olarak ise sizin kodunuzda anahtar olarak verdiğiniz ismi kullanır. Eğer karmaşık ve tahmin edilemez bir isim vermediğiniz taktirde cache dosyalarını bulmak çok zor olmayacaktır.

Aynı zamanda cache dosyalarında kullanıcının bilmemesi gereken hassas bilgiler bulunabilir. Bu hassas bilgiler bütün dünyaya açık ve ulaşılabilir olacaktır. Örnek olarak şu resim verilebilir:

![cache](/assets/genel/ci4-public/cache.png)

Kesinlikle bu dizini gizlemeniz gerekmektedir.

## `backups` Dizinini Dış Dünyaya Açıyorsunuz

Zaman zaman sisteminizin yedeğini alıp saklayabilirsiniz. Bu durumda muhtemelen yedeklerinizi `/writable/backups` dizini altında saklayacaksınız. Eğer backup dosyanızı _yedek.zip_, _yedek.rar_, _backup.zip_ tarzı tahmin edilebilir bir isim verdiğinizde sisteminizdeki önemli bilgiler yada dosyalar kullanıcının eline geçecektir. Sizde böyle bir şey olsun istemezsiniz dimi :)

## ENV Dosyası

Belkide en önemli kısım burası. Codeigniter önemli hassas bilgileri `.env` dosyasında saklamanıza imkan verir. Bu bilgiler veritabanı bilgileri, api keyleri gibi hassas bilgiler olabilir.

Herhangi bir nedenden dolayı public dizinini kaldırdığınız anda bu bilgilere isteyen herkes ulaşabilir. Aşağıdaki resimdeki gibi:

![env](/assets/genel/ci4-public/env.png)

Bu örnekte veritabanı bilgilerine ulaşabiliyoruz. Sanırım gerisini anlatmama gerek yok.

## Sonuç

Her ne yaparsanız yapın Codeigniter 4 teki public dizinini kaldırmayın. Bu dizin güvenlik açısından oldukça kritiktir.

Bu yazımda codeigniter 4 te public dizinini kaldırmanın tehlikelerinden bahsetmeye çalıştım. Umarım Faydalı olmuştur. Tekrar görüşmek üzere ...
