---
layout: post
title:  "Codeigniter 4 Kurulum"
Author: Süleyman ERGEN
tags: [Codeigniter4, ci4]
categories: [codeigniter4]
---

# Codeigniter 4 Kurulum

![ci4](/assets/img/codeigniter.png)

Herkese merhabalar.

Bildiğimiz üzere Codeigniter 4 geçtiğimiz haftalarda yayınlandı. Henüz Türkçe olarak pek bir anlatım olmadığı için ve biraz da bildiğim şeyleri başka insanlara aktarabilmek için kolları sıvadım. Elimden geldiğince bu yazımda Codeigniter 4 kurulumunu ve kurulum seçeneklerini anlatmaya çalışacağım.

## Kurulum

Codeigniter 4 birkaç farklı yöntemle kurulabilir: manuel, composer ile ve git kullanarak. Peki sizin için hangisi daha uygun?

* Eğer "indir ve devam et" şeklinde basit bir kurulum istiyorsanız (Codeigniter 3 deki gibi) manuel kurulum size göre.
* Eğer projenizde 3. parti kütüphaneler kullanmayı planlıyorsanız o zaman composer ile kurulumu tercih edebilirsiniz.
* Eğer bir geliştirici iseniz ve Codeigniter 4'e katkıda bulunmak istiyorsanız git kurulumunu tercih edebilirsiniz.

Codeigniter 4 kurulumuna geçmeden önce gereksinimleri karşıladığınızdan emin olmalısınız.

* Sisteminizde PHP'nin `7.2` ve üzeri bir sürümünün kurulu olduğundan emin olun.
* Şu eklentilerin kurulmuş olduğundan ve etkin olduğundan emin olun: `php-intl`, `php-json`, `php-mbstring`, `php-mysqlnd`, `php-xml`

Hazırsanız başlayabiliriz.

## Manuel Kurulum

Bu Codeigniter 4'ün en kolay kurulum seçeneğidir. Frameworkün ana github reposundan releases menüsünden [en son sürümü](https://github.com/CodeIgniter4/framework/releases/latest) indirebilirsiniz. Şu an itibariyle (2 nisan 2020) güncel sürümü `4.0.2` dir.

İndirdiğiniz zip dosyasını projenizin ana dizinine çıkarın. Ve kurulum tamam. Evet bu kadar basit :)

### Kurulumdan Sonra Proje Yapısı

Kurulum bittikten sonra proje dizininizde aşağıdaki dosya ve dizinlerin bulunması gerekiyor.

* `app/`
* `public/`
* `system/`
* `writable/`
* `spark`
* `env`

Bu dosya ve dizinler minimum kurulum için yeterlidir.

Peki zip dosyasında daha fazla dosya ve dizinler var. Onlar ne olacak ??? Bu dosya ve dizinler opsiyonel ve bazıları frameworkün reposuna ait olanlardır. Burayı biraz daha açayım.

Projenizde `phpunit` kullanarak test yazmayı düşünüyorsanız `tests/` dizinini ve `phpunit.xml.dist` dosyasını tutabilirsiniz.

* Eğer projenizde versiyon yönetimi için `git` kullanıyorsanız `.gitignore` dosyasını tutabilirsiniz.
* Projenizde composer kullanmayı düşünüyorsanız `composer.json` dosyasını tutabilirsiniz.
* Projenizde vagrant kullanmayı düşünüyorsanız `Vagrantfile.dist` dosyasını tutabilirsiniz.

Diğer dosya ve klasörler projenin reposu ile ilgilidir. Gönül rahatlığı ile silebilirsiniz.

### Güncellemeler

Projenizi manuel olarak kurdunuz ve frameworkün yeni bir sürümü yayınlandı. Bu durumda Projenizdeki `system` dizinini yeni sürümdeki `system` dizini ile değiştirmeniz gerekecektir.

Aynı zamanda yeni sürümle değişen config dosyaları ve içeriklerini düzenlemeniz gerekecek. Çünkü yeni sürümle yeni config dosyaları ve seçenekleri gelebilir.

### Dil Çeviri Kurulumu

Eğer sistem dil çevirilerinden faydalanmak istiyorsanız manuel kurulum ile aynı yöntemle projenize kurabilirsiniz.

Codeigniter 4 çevirilerinin [en son sürümünü](https://github.com/codeigniter4/translations/releases/latest) indirin. Zip dosysındaki `Language` dizininin içeriğini kendi projenizdeki `app/Languages` dizinine kopyalayın. Ve kurulum tamam.

Eğer sistem dil çevirilerine bir güncelleme gelirse ve yeni bir sürüm yayınlanırsa aynı kurulum adımlarını uygulayarak güncellemeyi uygulayabilirsiniz.

### Artıları

İndir ve devam et.

### Eksileri

Her güncelleme sonrasında çakışmalarla (conflict) ilgilenmelisiniz.

## Composer İle Kurulum

Bu kurulum projesinde çeşitli kütüphane kullanacaklar için uygundur.

Öncelikle sisteminizde composer yüklü olduğundan emin olun. Composer php için bir paket yöneticisidir. Şuradaki linkten sisteminize uygun olan sürümü kurabilirsiniz: [https://getcomposer.org/download/](https://getcomposer.org/download/) Eğer bir linux dağıtımı kullanıyorsanız terminalden aşağıdaki komutu root yetkisi ile çalıştırın.

```
$ sudo apt install composer
```

Bu şekilde composer kurulumunu tamamlayabilirsiniz.

### App Starter

Codeigniter 4 App Starter projenin ana iskelet yapısını barındırır. Composer ile yeni bir Codeigniter 4 projesi oluşturmak için kullanılan basit bir scripttir.

Terminalden şu komutla yeni bir proje oluşturulabilir.

```
$ composer create-project codeigniter4/appstarter proje-ismi
```

Bu komut `proje-ismi` ile bir dizin oluşturacak ve o dizin içerisine kurulumu yapalacaktır. Eğer phpunit ve diğer bazı bağımlılıkların kurulmasını istemiyorsanız `--no-dev` parametresini verebilirsiniz.

### Kurulumdan Sonra Proje Yapısı

Kurulum tamamlandıktan sonra proje dizininizde şu dosya/dizinleri görmeniz gerekiyor.

* `app/`
* `public/`
* `tests/`
* `writable/`
* `vendor/` composer paketlerinin ve Codeigniter 4 frameworkünün bulunduğu dizin
* `composer.json`
* `composer.lock`
* `phpunit.xml.dist`
* `spark`
* `builds`

Kurulum sonrasında repoya ait fazladan dosyalar göreceksiniz. Bunları gönül rahatlığıyla silebilirsiniz. Bunlardan bazıları license.txt, README.md, contributing.md ...

### Güncelleme

Eğer frameworke bir güncelleme gelirse terminalden şu komutu çalıştırın.

```
$ composer update
```

### Artıları

Kolay kurulum, kolay güncelleme.

### Eksileri

Her güncellemede `app/Config` dizinindeki değişiklikleri takip etmelisiniz.

### Dil Çeviri Kurulumu

Eğer sistem dil çevirilerini kullanmak istiyorsanız terminalden şu komutla kurabilirsiniz.

```
$ composer require codeigniter4/translations
```

Sistem dil çevirileri de composer paketleri ile kolay bir şekilde güncellenebilir. `composer update`.

## Git ile kurulum

__Bu kurulum yöntemi proje geliştirme için değil frameworke katkıda bulunmak (contributing) içindir.__ Lütfen bunun bilincinde olun!!!

Öncelikle Codeigniter 4'ün reposunu github hesabınızdan fork edin. Ardından fork ettiğiniz repoyu kendi bilgisayarınıza clone edin.

Repoyu kendi bilgisayarınıza çektikten sonra ana repo ile değişiklikleri senkronize etmek için şu komutu çalıştırın.

```
$ git remote add upstream https://github.com/codeigniter4/CodeIgniter4.git
```

### Güncelleme

İstediğiniz zaman şu komutlar ile güncellemeyi yapabilirsiniz.

```
git checkout develop
git pull upstream develop
git push origin develop
```

Her güncelleme sırasında çeşitli çakışmalar (conflict) meydana gelebilir. Bunları local olarak çözmeniz gerekecektir.

### Artıları

* Frameworkün son sürümünü kullanıyor olacaksınız. (henüz yayınlanmamış ve belki stabil olmayan)
* branch oluşturarak ve pull request oluşturarak frameworke katkıda bulunabilirsiniz.

### Eksileri

* Her güncelleme sırasında oluşan çakışmakarı (conflict) düzeltmek zorundasınız.
* Bu yöntemi proje geliştirme için kullanamazsınız.

### Dizin Yapısı

Git yöntemi ile kurulum yaptığınızda repoya ait bütün dizin ve dosyalar gelecektir. Bunları silmemelisiniz. Bunlar reponun birer parçasıdır.

### Sistem Dil Çevirileri

Eğer projenizi git ile kurduydanız sistem dil çevirilerini de github hesabınıza fork edip ardından kendi bilgisayarınıza çekmelisiniz. Unutmayın çeviriler için kullanılan repo ayrı, framework için kullanılan repo ayrıdır. Yani birbirinden tamamen bağımsızdır.

## Kurulumu Çalıştırma

Yukarıdaki kurulum yöntemlerinden birisiyle kulumu tamamladıktan sonra projeyi şu komutla çalıştırabilirsiniz.

```
$ php spark serve
```

![ci4-giris](/assets/genel/ci4-yayinlandi/ci4-giris.png)

## Bitirirken

Bu yazımda elimden geldiğince Codeigniter 4 kurulumlarını anlatmaya çalıştım. Gözünüze takılan bir hata gördüyseniz yahut anlaşılmayan bir adım var ise dürtmeyi unutmayın. Evde kalın, sağlıcakla kalın.
