---
layout: post
title:  "Wazuh Log Yönetimi"
tags: [security, wazuh, devsecops, siem]
categories: [security]
---

Eğer SIEM ürünü olarak wazuh kullanıyorsanız ve log boyutunuz ciddi oranda artmaya başladıysa doğru yerdesiniz. Bu yazıda Wazuh'ta log yönetimine ait faydalı bilgiler bulacaksınız. Keyifli okumalar.

![Wazuh-Logo](../../assets/security/wazuh-log-retention/wazuh-logo.png)

## Wazuh Logları

Wazuh kendi içerisinde çeşitli loglar üretmektedir. Bunlar event logları ve ossec logları olarak ikiye ayrılabilir.

1. Event logları agentlardan gelen loglardan üretilen alertlere ait loglar ve arşiv (archive) loglarıdır.
2. Wazuh/ossec logları ise wazuh-managerin kendi ürettiği loglardır.

Şimdi bu logları yakından tanıyalım.

### Ossec Logları

Wazuh/Ossec logları wazuh managera ait bileşenler/servisler tarafından üretilen loglardır. Örneğin authd, logcollerd, vulnerability-detector, remoted servisleri gibi.

Wazuh bu loglara ait retention ve rotation işlemlerini yönetebilir. Yani bu loglar wazuh tarafından düzenli olarak arşivlenip saklanabilir ve sonrasında silinebilir. Ossec logları `/var/ossec/logs/ossec.log` dosyasında tutulur ve `/var/ossec/logs/wazuh/` dizini altında arşivlenir.

Wazuh konfigürasyon dizininde (`/var/ossec/etc/`) `local_internal_options.conf` dosyasından bu ayarlar düzenlenebilir. Default saklama süresi 30 gündür. 30 günden eski loglar silinir. Disk alanına veya log saklama gereksinimlerinize bağlı olarak bu süreyi istediğiniz gibi değiştirebilirsiniz.

### Wazuh Event Logları

Wazuh event logları agentlardan gelen loglar ve bu loglardan üretilen alert loglarıdır. Wazuh, alert ve arşiv loglarını günlük olarak rotate eder (yönlendirir). Bu loglar aşağıdaki dizinlerdedir.

- `/var/ossec/logs/alerts/`
- `/var/ossec/logs/archives/`

Bu dizinlerin altında Yıl/Ay formatında tutulur.

#### Wazuh Alert Logları

Bu loglar wazuhun agentlardan aldığı loglardan ürettiği alertlerdir. Eğer bir log bir kuralı tetikliyor ise alert üretilir ve bu alert `/var/ossec/logs/alerts/alerts.log` dosyasında saklanır. Ardından her gün bu loglar rotate edilir. Yani her gün için farklı bir dosyada saklanır. Bu logları `/var/ossec/logs/alerts/` altında `{Yıl}/{Ay}/ossec-alerts-{GUN}.log` formatında bulabilirsiniz.

Örnek bir çıktı aşağıdaki gibidir.

```
[root@wazuh-server alerts]# ls -alh /var/ossec/logs/alerts/
drwxr-x--- 4 wazuh wazuh   28 Sep  2 11:19 2023
-rw-r----- 2 wazuh wazuh 639K Sep 23 07:23 alerts.json
-rw-r----- 2 wazuh wazuh 414K Sep 23 07:23 alerts.log

[root@wazuh-server alerts]# ls -alh /var/ossec/logs/alerts/2023/Sep/
-rw-r----- 2 wazuh wazuh 639K Sep 23 07:23 ossec-alerts-23.json
-rw-r----- 2 wazuh wazuh 414K Sep 23 07:23 ossec-alerts-23.log
```

Wazuh bu logları düzenli olarak **silmez**. Bu işlem için kendimiz çözüm üretmek zorundayız.

Alert logları wazuh tarafından düzenli olarak filebeat yardımı ile elasticsearch clustırınıza yönlendirilir. Bu logları `wazuh-alerts` indeksi altında inceleyebilirsiniz.

#### Wazuh Archive Logları

Arşiv logları Wazuha agentlardan gelen bütün loglardır. Alert loglarının aksine bir alert üretmedese saklanabilir.

Wazuh arşiv logları default olarak kapalı gelmektedir. Yani bir log alert üretmiyor ise kaydedilmemektedir. Bu logları etkinleştirmek için `/var/ossec/etc/ossec.conf` dosyasında `logall` konfigirasyonundan `yes` olarak işaretlenmelidir. Veya kibana wazuh uygulamasından konfigürasyon sekmesinden düzenlenebilir.

Arşiv loglarını `/var/ossec/logs/archives/` dizini altında bulabilirsiniz. Bu dizin altında `YIL/AY` formatında saklanmaktadır.

```
[root@wazuh-server archives]# ls -alh /var/ossec/logs/archives
drwxr-x--- 4 wazuh wazuh   28 Sep  2 11:19 2023
-rw-r----- 2 wazuh wazuh 1.3M Sep 23 07:17 archives.json
-rw-r----- 2 wazuh wazuh 586K Sep 23 07:17 archives.log

[root@wazuh-server archives]# ls -alh /var/ossec/logs/archives/2023/Sep
-rw-r----- 2 wazuh wazuh 1.3M Sep 23 07:22 ossec-archive-23.json
-rw-r----- 2 wazuh wazuh 590K Sep 23 07:22 ossec-archive-23.log
```

Arşiv logları default olarak ELK clustırınıza yönlendirilmez! Eğer bu logların tamamını Kibana üzerinden görüntüleyebilmek veya sorgulayabilmek istiyorsanız filetbeat konfigürasyonunda arşiv loglarını etkinleştirmeniz gerekiyor. Bu işlem için `/etc/filebeat/filebeat.yml` dosyasında `archives.enabled` ayarını `true` yapabilirsiniz.

```yml
filebeat.modules:
  - module: wazuh
    alerts:
      enabled: true
    archives:
      enabled: true # <-- default false, true yap
```

Bu işlemden sonra wazuh-manageri yeniden başlatın. Ardından kibana arayüzünden `Stack Management` bölümünden `wazuh-archives-*` isminde yeni bir indeks-pattern oluşturun. Ardından discover bölümünden indeksi görebilirsiniz.

![Alt text](../../assets/security/wazuh-log-retention/archives-pattern.png)

## Log Retention Nedir?

Log retention, bir sistemlerin ürettiği logları ne kadar süre saklanacağını belirleyen bir kavramdır. Logların doğru bir şekilde saklanması güvenlik, sorun giderme ve yasal uyumluluk için önemlidir.

Örneğin bir güvenlik ihlalinin incelenmesi için loglar kritik öneme sahiptir. Logların bulunmaması veya saklanmaması durumunda güvenlik ihlalinin nedeni bulunamayabilir.

Veya uyulması gereken yasal bir düzenleme olabilir. Örneğin ödeme altyapısı sağlayan kurumlar için PCI-DSS uyumluluğu zorunludur. Bu kurumlar PCI-DSS standartlarına göre loglarını belirli bir süre (3 ay hot, 12 ay cold) saklamalıdır.

Bu süre geçtikten sonra loglar silinebilir. Bu sayede gereksiz maliyetten kaçınılmış olur.

## Wazuh Log Retention Neden Gerekli?

Wazuh'a bağlanan cihaz sayısına göre ve syslog miktarına göre log dosyaları oluşmaktadır. Bu logları yasal düzenlemelere göre veya kurumun ihtiyaçlarına göre belirli bir süre saklanmalıdır. Ardından loglar silinebilir.

Bu log dosyaları wazuh-manager'da veya elastic clustırınızda büyük miktarda disk alanı kullanabilir. Bunun önüne geçmek için log retention uygulanmalıdır.

Disk alanı sonsuz değil, bir maliyeti var. Bu nedenle maliyet-etkin bir log yönetimi oluşturulmalıdır.

## Wazuh Log Retention

Wazuh alert ve archive loglarını belirli bir günden sonra silmez. Manuel veya cron yardımı ile silinmesi gerekir.

Eğer wazuh-manager kurulumunu bir sanal makine üzerine yaptıysanız cronjob kullanabilirsiniz. Aşağıdaki cronlar her gün saat 2 de 30 günden ekli logları silebilirsiniz. Veya uzaktaki bir sunucuya veya s3 bucketınıza yönlendirebilirsiniz.

```sh
0 2 * * * find /var/ossec/logs/alerts/ -type f -mtime +30 -exec rm -f {} \;
0 2 * * * find /var/ossec/logs/archives/ -type f -mtime +30 -exec rm -f {} \;
```

Bu satırları eklemek için root kullanıcısı ile `crontab -e` komutu ile cron düzenlemesi yapabilirsiniz.

Eğer wazuh kurulumunu k8s ile yaptıysanız pod içerisinde cron kullanamayacaksınız. Bunun yerine k8s de tanımlı olan cron özelliğini kullanabilirsiniz.

```yml
apiVersion: batch/v1
kind: CronJob
metadata:
  name: wazuh-log-retention
spec:
  schedule: "0 2 * * *" # at 2am every day # production
  jobTemplate:
    spec:
      template:
        spec:
          restartPolicy: Never
          containers:
          - name: wazuh-log-deleter
            image: ubuntu
            command: ["/bin/bash", "-c"]
            args:
            - |
              ehco "Starting waruh log deletion script! Deleting old logs."
              find /var/ossec/logs/alerts/ -type f -mtime +30 -exec rm -f {} \;
              find /var/ossec/logs/archives/ -type f -mtime +30 -exec rm -f {} \;
            volumeMounts:
            - name: wazuh-log-data
              mountPath: "/var/ossec/logs/"
          volumes:
          - name: wazuh-log-data
            persistentVolumeClaim:
              claimName: wazuh-log-data
```

Sonuç olarak logları ihtiyacınıza göre bir süre saklamanız ve ardından silmeniz gerekmektedir.

## Elastic Rog Retention

Wazuh alert logları düzenli olarak elastic clustırına yönlendirir. Kibana üzerinden görünen loglar da ELK üzerinden sorgulanır.

Elasticsearch Index Lifecycle (ILM), Elasticsearch log yönetimi için kullanılan bir özelliktir. ILM, indekslerin otomatik olarak oluşturulması, sıkıştırılması, taşınması ve silinmesi gibi işlemleri otomatize eder.

Elasticsearch'de bir index log yönetimi için 4 farklı aşama tanımlanmıştır.

1. **Hot:** İndeksler başlangıçta bu aşamada başlar. Bu aşamada yeni veriler indekslere yazılır ve veri hızla güncellenir veya sorgulanır. Bu aşamada için yüksek performans çok önemlidir. ELK da bu aşama zorunludur.
2. **Warm:** Bu aşamada veriler artık aktif olarak güncellenmez. İndeksler bu aşamada sıkıştırılabilir ve veri boyutunu küçültebilir.
3. **Cold:** Bu aşamada, verilerin daha az erişildiği veya sorgulandığı durumlar için optimize edilmiş indekslere taşınır. Bu aşamada indeksleri daha fazla sıkıştırma uygulanır ve düşük maliyetli depolama kullanma seçeneği vardır.
4. **Delete** İndekslerin belirli bir süre sonra silinmesi gerektiğinde bu aşama gerçekleşir. Veriler silinir.

ELK'da log yönetiminin efektif yapılabilmesi için log policy kullanılabilir. Elasticsearch `wazuh-alerts-*` indeksi için log retention süreleri şu şekilde olabilir:

- Hot Phase: Aktif log içindir. Indeksler ilk 30 gün bu aşamada bulunur.
- WARM Phase: İndeksler 30 günden sonra warm durumda saklanır.
- COLD Phase: 180 gün. İndeksler 180 günden sonra bu aşamada saklanır. Arşiv loglarıdır. Aktif arama için değildir.
- Delete Phase: 365 gün. 365 günden eski loglar silinir.

![Alt text](../../assets/security/wazuh-log-retention/elk-ilp.png)

ELK index yönetimine dair daha fazlası için: [ILM: Manage the index lifecycle](https://www.elastic.co/guide/en/elasticsearch/reference/7.17/index-lifecycle-management.html)

## EOF

Bu yazıda Wazuh log retention üzerine tartıştık. Umarım faydalı olmuştur. Sonra görüşmek üzere . . .
