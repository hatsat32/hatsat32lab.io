---
layout: post
title:  "DevSecOps ve Shift Left Security"
tags: [security, appsec, sdlc, devsecops]
categories: [security]
---

![Untitled](../../assets/security/devsecops-ve-shift-left-sec/devsecops.png)

Bir yazılım geliştirmek kolay bir iş değildir. Bu yazılımı güvenli bir şekilde geliştirmek ise tahmin ettiğinizden zor olabilir. Fakat doğru yöntemler kullanılarak güvenli yazılımlar geliştirebiliriz.

Bu rehberde Devsecops ve shift left security kavramlarını inceleyeceğiz.

Keyifli okumalar.

## Geleneksel Yazılım Güvenliği

Geleneksel yazılım güvenliğinde genel olarak sızma testi (pentest) ve bug bounty programları öne çıkar. Bu yöntemlerin avantajları olsada eksik kaldıkları birçok yer vardır.

Bir zamanlar bir uygulama geliştirildiğinde, sonrasında sızma testi ile zafiyetlerin tamamının bulunabildiği düşünülürdü. Fakat zamanla bu kanının tamamen yanlış olduğu defalarca kez kanıtlandı.

Bununla beraber bu yöntemde diğer bazı sorunlar var. Bunları temel olarak şu şekilde sıralayabiliriz:

- Pentest'in belirli bir zaman limiti vardır. Örneğin 1 veya 2 hafta gibi. Hiç bir kurum aylarca sistemlerine ve uygulamalarına sızma testi yaptıramaz veya yaptırmaz.
- Sızma testleri genellikle projenin son aşamalarında yapılır. Ya canlıya çıkmadan veya canlı ortamda. Bu durumda bulunan zafiyetlerin düzeltilmesi oldukça maliyetli olabilir. Buraki maliyet olarak parayı kastetmiyorum. Harcanan emek ve zaman.
- Sızma testi sızma testini yapan kuruma ve bu kurumun personeline oldukça bağlıdır. Her kurumun veya personelin yaptığı pentest aynı olmayacaktır. Aynı şekilde sızma testini yapan personelin o günkü ruh hali de sızma testini etkileyecektir.
- Sızma testi maliyetlidir.

Sızma testine ek olarak diğer bir yaklaşım ise "Bug bounty" programı yürütmektir. Bu yöntemin artıları olduğu gibi dezavantajları vardır. Örneğin bu yöntem proje canlıya çıktıktan sonra yapılabilir. Yani canlıya çıkmadan zafiyetler testpit edilemez.

Bütün bunlar ile beraber iyi bir sızma testinin yapıldığını ve raporun kuruma iletildiğini düşünelim. Bu noktada bulunan zafiyetlerin düzeltilmesi ne kadar vakit alır? Rapor geldikten sonra 1 ay içerisinde hepsi düzeltilebilir mi? Pek mümkün değil çünkü canlıya çıkmış bir koddaki hatayı düzeltmek kolay olmayacakdır.

## Modern Uygulama Güvenliği

Bütün bu olumsuzluklar bizi "geleneksel uygulama güvenliği"nin yeterli olmadığını gösterdi. Yani uygulama güvenliği sadece sızma testi ve bugbounty ile sağlanabilecek bir şey değil. Uygulama geliştirileye başlar başlamaz güvenliğin sürece dahil edilmesi gerekiyor. Buna ise Devsecops veya Secure SDLC denir.

## Secure SDLC ve DecSecOps'a Giriş

Uygulama güvenliğinde güvenliğin bütün sürecin bir parçası olması gerektiğini söylemiştik. Peki bunu nasıl yaparız. Bir uygulama geliştirilirken planlama, tasarım, kodlama, test gibi çeşitli süreçlerden geçer:

![Untitled](../../assets/security/devsecops-ve-shift-left-sec/sdlc.png)

İşte bu süreçlerin hepsinde bir şekilde güvenliği kaynaştırmamız gerekiyor. İşte bu nokdata yapabileceklerinizden bazıları şunlar.

### Secure Code Training

Öncelikle güvenli yazılım geliştirmenin ne olduğu ve nasıl sağlanabileceğini bilmek gerekir. Bunun için çeşitli ücretli ve ücretsiz kurslardan, internetteki yazılı kaynaklardan faydalanabilirsiniz. Ben örnek olması açısından bir kaç tane bırakayım:

- Ulaştırma ve Altyapı Bakanlığı Si̇vi̇l Havacılık Genel Müdürlüğünün hazırlamış olduğu [Güvenli Yazılım Geliştirme Rehberi](https://web.shgm.gov.tr/documents/sivilhavacilik/files/pdf/duyuru/2021/HGD/HGD-GYGR.pdf) güzel bir başlangıç olabilir.
- Veya PortSwiggerın hazırlamış olduğu [Web Güvenliği Akademisi](https://portswigger.net/web-security) oldukça geniş ve kapsamlı bir kaynak.
- Owasp tarafından hazırlanan belkide en iyi uygulama güvenliği rehberi: [OWASP Cheat Sheet Series](https://cheatsheetseries.owasp.org/)

### Tehdit modelleme (Thread Modeling)

Tehdit modelleme bir uygulama geliştirilmeye başlanmadan uygulamada oluşabilecek zafiyetleri oluşmadan önlemeyi amaçlayan bir yaklaşımdır.

Bu şekilde anlatınca biraz soyut kalabilir. Bunu basit bir örnekle biraz somutlaştıralım. Örneğin bir web uygulaması olduğunu varsayalım. Bu web uygulaması kullandığı verileri saklamak için bir veritabanı kullanacak olsun. Doğal olarak veritabanı ile işlemler yaparken SQL kullanılacaktır. Bu durumda uygulamada SQL Injection zafiyeti görülebilir. Bunu önlemek için ise prepared statement denilen yapılar kullanılabilir. Bu sayede bu zafiyet engellenebilir.

Bu örnekteki gibi tehdit modellemesi ile zafiyetler çıkadan engellenmeye çalışılır.

Bu basit örnek gerçek bir uygulama için elbette yeterli olmayacaktır. Bu nedenle gerçek projelerde kullanabileceğiniz bazı araçlar var.

Microsoft thread modeling tool güzel bir örnek. Bu uygulama ile çıkabilecek zafiyetleri ve tehditleri öngörebilirsiniz ve gerekli önlemleri alabilirsiniz.

![Untitled](../../assets/security/devsecops-ve-shift-left-sec/micro.png)

Link: [https://learn.microsoft.com/en-us/azure/security/develop/threat-modeling-tool](https://learn.microsoft.com/en-us/azure/security/develop/threat-modeling-tool)

Eğer tehdit modelleme ile ilgili daha fazla şey öğrenmek isterseniz OWASP'ın hazırlamış olduğu dödümanları inceleyebilirsiniz:

- [OWASP Threat Modeling](https://owasp.org/www-community/Threat_Modeling)
- [Threat Modeling Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/Threat_Modeling_Cheat_Sheet.html)

### Auth Matrix

Auth Matrix kullanıcıların ve kullanıcı rollerinin uygulama üzerindeki kaynaklara nasıl ve hangi yetki ile erişebileceğini gösteren bir tablodur. Bu tablo yetki zafiyetlerini engellemek için etkili bir araç olabilir.

![Untitled](../../assets/security/devsecops-ve-shift-left-sec/auth.png)

Böyle bir tablonun olmaması durumunda uygulamada yetki hatalarının olması kaçınılmaz olacaktır.

### IDE Eklentileri

Bir uygulama geliştirirken tam geliştirme esnasında bazı zafiyetler tespit edilebilir. Bunun için kullandığınız IDE için geliştirilen güvenlik eklentilerini kullanabilirsiniz. Örnek vermek gerekirse vscodda security diye arattığınızda kullanabileceğiniz çok güzel eklentiler çıkacaktır.

![Untitled](../../assets/security/devsecops-ve-shift-left-sec/vscode-sec-plugin.png)

Örneğin synack security eklentisi bağımlılıkların zafiyetlerini oldukça şık bir şekilde IDE (vscode) ortamında gösteriyor.

![Untitled](../../assets/security/devsecops-ve-shift-left-sec/vscode-sec-plugin-2.png)

### Pre Hook Hookları

Commit işlemi gerçekleşmeden hemen önce güvenlik kontrollerini hooklar aracılığı ile ekleyebiliriz. Bu sayede bir zafiyet ile karşılaşılması durumunda commit işlemi iptal edilecektir ve zafiyetli kod git historisine geçmeyecektir.

Pre-Commit hookları ile yapabileceğiniz bir kaç şey:

- `gitleaks` ile kaynak koddaki hardcoded bilgileri tespit edebilirsiniz.
- `github.com/sirwart/ripsecrets` hardcoded bilgileri tespit etmek için
- `python-safety-dependencies-check` ile python kütüphanelerindeki bilinen zafiyetlerin tespiti
- Local statik kod analizi
- ve daha fazlası . . . . .

Bu ve buna benzer araçlar kullanarak zafiyetli kodun commit edilmesini engelleyebilirsiniz. Daha fazlası için aşağıdaki linkleri inceleyebilirsiniz:

- [https://pre-commit.com/hooks.html](https://pre-commit.com/hooks.html)
- [OWASP DevSecOps Guideline - v-0.2 Pre Commit](https://owasp.org/www-project-devsecops-guideline/latest/01-Pre-commit)

### Commit İmzalama

Commit imzalama yazılan bir kodun doğru bir developer tarafından geldiğini onaylamak için güzel bir yöntemdir. Eğer yubikey gibi bir güvenlik anahtası kullanıyorsanız bu yöntemi kullanmak oldukça faydalı olacaktır.

Öncelikle imzalama işlemi için bir private/public anahtar çifti oluşturmanız gerekiyor. Ardından git komutu ile ve oluşturduğunuz bu private key ile commitinizi imzalayabilirsiniz. Sonrasında public key ile onaylayabilir siniz. Public keyinizi keyserverlara atmayı unutmayın.

- [Kernel Maintainer PGP guide](https://www.kernel.org/doc/html/v4.19/process/maintainer-pgp-guide.html)
- [YubiKey GPG Guide](https://github.com/drduh/YubiKey-Guide)

### Software Composition Analysis

Bir uygulama geliştiriken herşeyi kendimiz yazmayız. Daha önceden başkaları tarafından yazılmış kütüphaneleri ve kodları kullanırız. Bu bize zaman ve maliyet avantajı sağlar.

Bununla beraber, sizin yazdığınız kodda nasıl zafiyet çıkabilirse, bu kütüphanelerde de zamanla zafiyetler çıkabilmektedir. Bu sizin projenizi de etkileyecektir. Sonuçta başkasının yazdığı kodu kendi kodunuza dahil ediyorsunuz. Bu nedenle 3. parti kütüphaneleri bilenen zafiyetlere karşı taramak ve gerektiğinde hızlı bir şekilde güncelleyebilmek önemlidir.

Bunun için SCA (Software Composition Analysis) dediğimiz araçlardan faydalanırız. Bunlardan bazıları şunlardır:

- [Dependabot](https://github.com/dependabot): eğer github kullanıyorsanız oldukça pratik bir araç.
- [OWASP Dependency Check](https://owasp.org/www-project-dependency-check/)

Böyle bir aracın kullanılması gelecekte log4j gibi bir zafiyet çıkması durumunda oldukça faydalı olacaktır.

### CI/CD Süreçleri

Projelerimizde unit testleri çalıştırmak, kod kalitesi testlerini çalıştırmak, build işlemlerini gibi işlemler için CI/CD kullanırız. Elbette güvenlik testleri için de kullanabiliriz.

CI/CD süreçlerine güvenlik testlerini dahil edebilmeniz için bazı araçlar:

- Öncelikle statik kod analizi ile başlayabiliriz. Sonarqube, semgrep gibi araçları workflowlarınıza dahil ederek kaynak kodunuzu analiz edebilirsiniz.
- `gitleaks` gibi araçları ekleyerek kaynak koddaki gizli kalması gereken bilgileri tespit edebilirsiniz.
- Bağımlılıklarınızı tarayabilir ve bilinen bir zafiyet çıkması durumunda hemen haberdar olabilirsiniz.
- `ZAP` veya `Dastardly` gibi araçlar ile dinamik kod analizini gerçekleştirebilirsiniz.

Elbette bunlar yapabileceklerinizden sadece birkaçı. İhtiyacınıza göre bu testleri arttırabilir veya azaltabilirsiniz. Şu kaynakları incelemeyi unutmayın:

- [https://kondukto.io/integrations](https://kondukto.io/integrations)

### Monitoring

Uygulamamızı canlıya aldık diyelim. Bu durumda internete açılan her uygulamada olduğu gibi bizim uygulamamızda çeşitli saldırılara maruz kalacaktır. Bu durumda uygulamamızı ve uygulamamızın deploy edildiği platformu (VM, k8s, docker …) monitor etmemiz gerekir.

Uygulamamızda bir loglama mekanizması olması faydalı olacaktır. Kullanıcıların yaptığı çeşitli işlemleri loglamak önemli. Örneğin kullanıcıların hatalı parola denemelerini, yetkisiz erişim denemelerini (403), JWT decode hatalarını vs loglayabilirsiniz. Sadece loglamak yetmez, bu logları inceleme ve görselleştirme yapabileceğiniz ELK gibi bir ortama göndermelisiniz. Bu işlemleri yaptığınızda uygulamada oluşabilecek anormal davranışları tespit edebilmeye başlayacaksınız.

Her web uygulamasının olmaz ise olmazı olan bir WAF kullanmak faydalı olacaktır. Modsecurity kullanarak başlayabilirsiniz. Ardından modsecurity loglarını Wazuh gibi bir siem ürününe göndererek bir saldırıdan anlık olarak haberdar olabilirsiniz. Elbette bu iki araç ücretsiz ve özgür yazılım.

Eğer uygulamanızı k8s üzerinde çalıştırıyor iseniz falco gibi bir araç ile container içerisindeki anormal davranışları tespit edebilirsiniz. Bu sayede gerçekleşen bir saldırı zamanında tespit edilip gerekli önlemler alınabilir.

Ve bu liste böyle uzar gider.

Burada önemli olan uygulama canlıya alındıktan sonra bir şekilde monitör edilmesidir.

## Nereden Başlayabilirim

Bunlar çok fazla, hepsini nasıl yapacağım diyorsan şu dökümanları inceleyebilirsin:

- [https://owasp.org/www-pdf-archive//OWASP_Quick_Start_Guide.pdf](https://owasp.org/www-pdf-archive//OWASP_Quick_Start_Guide.pdf)
- [https://github.com/6mile/DevSecOps-Playbook](https://github.com/6mile/DevSecOps-Playbook)

Başlangıç için bir fikir vereceklerdir.

## EOF

Bu yazıda DevSecOps ve shift left security üzerine tartıştık. Bu rehberdeki adımları uygulayarak güvenliği sola kaydırabilir veya soldan başlatabilirsiniz. Umarım faydalı olmuştur. Sonra görüşmek üzere . . .

## Referanslar

- [https://mvsp.dev/](https://mvsp.dev/)
- [https://github.com/6mile/DevSecOps-Playbook](https://github.com/6mile/DevSecOps-Playbook)
